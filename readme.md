# Erasys Matcher
This is a small application to find a match based on Erasys user properties.

To run clone the repository and:
```
npm install
npm run typings
npm run dev
```
then browse `http://localhost:3000`