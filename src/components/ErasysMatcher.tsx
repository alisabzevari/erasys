import * as React from "react";
import AppBar from 'material-ui/AppBar';

import SearchBar from './SearchBar';
import UserTable from './UserTable';
import { User } from '../models/User';
import { ErasysDimensions } from '../models/ErasysDimensions';

interface ErasysMatcherState {
  users: User[];
  dimensions: ErasysDimensions;
}

export default class ErasysMatcher extends React.Component<{}, ErasysMatcherState> {
  rawData: User[];
  ndx: CrossFilter.CrossFilter<User>;

  constructor(props: {}, context: any) {
    super(props, context);
    this.state = {
      dimensions: null,
      users: []
    }
  }

  private flatten(data: any[]): User[] {
    return data.map(d => ({
      id: d.id,
      name: d.name,
      avatarUrl: d.preview_pic.url,
      originalData: d,
      area: d.location.area,
      city: d.location.city,
      country: d.location.country,
      age: d.personal.age,
      bodyHair: d.personal.body_hair,
      bodyType: d.personal.body_type,
      ethnicity: d.personal.ethnicity,
      eyeColor: d.personal.eye_color,
      height: d.personal.height.cm,
      relationship: d.personal.relationship,
      smoker: d.personal.smoker,
      weight: d.personal.weight.kg,
      analPosition: d.sexual.anal_position,
      favoredPosition: d.sexual.favored_position,
      saferSex: d.sexual.safer_sex,
      sm: d.sexual.sm
    }));
  }

  private initAnalyzer(data: any[]) {
    this.rawData = this.flatten(data);
    this.ndx = crossfilter(this.rawData);
    let dimensions: ErasysDimensions = {
      id: this.ndx.dimension(d => d.id),
      bodyHair: this.ndx.dimension(d => d.bodyHair),
      analPosition: this.ndx.dimension(d => d.analPosition),
      ethnicity: this.ndx.dimension(d => d.ethnicity),
      bodyType: this.ndx.dimension(d => d.bodyType),
      eyeColor: this.ndx.dimension(d => d.eyeColor),
      relationship: this.ndx.dimension(d => d.relationship),
      smoker: this.ndx.dimension(d => d.smoker),
    };
    this.setState({
      dimensions: dimensions,
      users: []
    });
  }

  handleFilterChange() {
    this.setState({
      dimensions: this.state.dimensions,
      users: this.state.dimensions.id.top(Infinity)
    });
  }

  componentDidMount() {
    fetch('user_data.json')
      .then(response => response.json())
      .then(data => this.initAnalyzer(data.items))
      .catch(err => console.error(err));
  }

  render() {
    return (
      <div>
        <AppBar title="Erasys Matcher"/>
        <div className="grid">
          <div className="col-1-1">
            <SearchBar dimensions={this.state.dimensions} onChange={this.handleFilterChange.bind(this) } />
          </div>
          <div className="col-1-1">
            <UserTable users={this.state.users} />
          </div>
        </div>
      </div>
    );
  }
};