import * as  React from 'react';
import { List } from 'material-ui/List';
import Paper from 'material-ui/Paper';
import Subheader from 'material-ui/Subheader';
import { grey400, darkBlack, lightBlack } from 'material-ui/styles/colors';

import { User } from '../models/User';
import UserItem from './UserItem';

const style = {
  paper: {
    width: '100%',
  }
};

export interface UserTableProps {
  users: User[];
}

export default class UserTable extends React.Component<UserTableProps, {}>{
  render() {
    if (!this.props.users || this.props.users.length == 0)
      return null;
    const listItems = this.props.users.slice(0, 10).map(user => (<UserItem user={user}></UserItem>));

    return (
      <Paper style={style.paper} zDepth={2}>
        <List>
          <Subheader>Users ({this.props.users.length}) </Subheader>
          {listItems}
        </List>
      </Paper>
    )
  }
}