import * as  React from 'react';
import { ListItem } from 'material-ui/List';
import { grey400, darkBlack, lightBlack, blue400 } from 'material-ui/styles/colors';
import Avatar from 'material-ui/Avatar';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import FavoriteIcon from 'material-ui/svg-icons/action/favorite';

import { User } from '../models/User';

const iconButtonElement = (
  <IconButton
    touch={true}
    tooltip="Match"
    tooltipPosition="bottom-left">
    <FavoriteIcon color={blue400} />
  </IconButton>
);

export interface UserItemProps {
  user: User;
}

export default class UserItem extends React.Component<UserItemProps, {}> {
  render() {
    return <ListItem
      leftAvatar={<Avatar src={this.props.user.avatarUrl} />}
      rightIconButton={iconButtonElement}
      primaryText={this.props.user.name}
      secondaryText={
        <span>
          <p>{this.props.user.country} - {this.props.user.city}</p>
        </span>
      }
      secondaryTextLines={2}
      />
  }
}