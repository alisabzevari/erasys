import * as  React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import { ErasysDimensions } from '../models/ErasysDimensions';

export interface SearchBarProps {
  dimensions: ErasysDimensions;
  onChange: () => void;
}

interface SearchBarState {
  [index: string]: string;
  bodyHair: string;
  analPosition: string;
  ethnicity: string;
  bodyType: string;
  eyeColor: string;
  relationship: string;
  smoker: string;
}

export default class SearchBar extends React.Component<SearchBarProps, SearchBarState> {
  private selectFieldNames: SearchBarState;

  constructor(props: any, context: any) {
    super(props, context);
    this.selectFieldNames = {
      bodyHair: "Body Hair",
      analPosition: "Anal Position",
      ethnicity: "Ethnicity",
      bodyType: "Body Type",
      eyeColor: "Eye Color",
      relationship: "Relationship",
      smoker: "Smoker"
    };

    this.state = {
      bodyHair: "Body Hair",
      analPosition: "Anal Position",
      ethnicity: "Ethnicity",
      bodyType: "Body Type",
      eyeColor: "Eye Color",
      relationship: "Relationship",
      smoker: "Smoker"
    };
  }

  handleChange(who: string, event: any, index: any, value: any) {
    this.props.dimensions[who].filter(value);
    this.props.onChange();
    this.state[who] = value;
    this.setState(this.state);
  }

  render() {
    if (this.props.dimensions === null)
      return null;
    let selectFields = Object.keys(this.props.dimensions).filter(dim => dim !== 'id').map(dim => (
      <div className="col-1-3">
        <div className="content">
          <SelectField floatingLabelText={this.selectFieldNames[dim]} value={this.state[dim]} onChange={this.handleChange.bind(this, dim) }>
            {this.props.dimensions[dim].group().all().map(g => (<MenuItem key={g.key} value={g.key} primaryText={g.key} />)) }
          </SelectField>
        </div>
      </div>
    ));

    return (
      <div className="col-1-1">
        {selectFields}
      </div>
    );
  }
};