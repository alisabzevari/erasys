export interface User {
  id: string;
  name: string;
  avatarUrl: string;
  area: string;
  city: string;
  country: string;
  age: number;
  bodyHair: string;
  bodyType: string;
  ethnicity: string;
  eyeColor: string;
  height: number;
  relationship: string;
  smoker: string;
  weight: number;
  analPosition: string;
  favoredPosition: string;
  saferSex: string;
  sm: string;
  originalData: any;
}