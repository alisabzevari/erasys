import { User } from './User';

export interface ErasysDimensions {
  [index: string]: CrossFilter.Dimension<User, string>;
  id: CrossFilter.Dimension<User, string>;
  bodyHair: CrossFilter.Dimension<User, string>;
  ethnicity: CrossFilter.Dimension<User, string>;
  bodyType: CrossFilter.Dimension<User, string>;
  eyeColor: CrossFilter.Dimension<User, string>;
  relationship: CrossFilter.Dimension<User, string>;
  smoker?: CrossFilter.Dimension<User, string>;
  analPosition?: CrossFilter.Dimension<User, string>;
  favoredPosition?: CrossFilter.Dimension<User, string>;
  saferSex?: CrossFilter.Dimension<User, string>;
  sm?: CrossFilter.Dimension<User, string>;
}
